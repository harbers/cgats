# cgats

## Read, write, and manipulate [CGATS color files](http://www.colorwiki.com/wiki/CGATS.17_Text_File_Format)

This crate is a library intended to read, write, and transform CGATS color
files and is currently in development. Currently this library only supports
ASCII text formats, but intends to add support for XML soon. The included
binary is a simple implementation of the library's API.

### What's new in v0.2

* Complete rewrite with heavy reliance on iterators for reading and
  manipulating data.
* Many new methods for iterating over data and converting betweeen types.
* New example binary with added features.

[Rust API Documentation](https://docs.rs/cgats)

Or build the documentation yourself:

```sh
cargo doc --open
```

### The CGATS format

Here is a basic [CGATS color file](test_files/cgats1.tsv):

```tsv
CGATS.17
BEGIN_DATA_FORMAT
SAMPLE_ID	SAMPLE_NAME	CMYK_C	CMYK_M	CMYK_Y	CMYK_K
END_DATA_FORMAT
BEGIN_DATA
1	Cyan	100	0	0	0
2	Magenta	0	100	0	0
3	Yellow	0	0	100	0
4	Black	0	0	0	100
5	Blue	100	100	0	0
6	Red	0	100	100	0
7	Green	100	0	100	0
8	3cBlack	100	100	100	0
9	4cBlack	100	100	100	100
10	3cGray	50	40	40	0
11	1cGray	0	0	0	50
END_DATA
```

There are several more exmples in the [test_files](test_files) directory. The
CGATS format is similar to TSV, but with a few additions. The
`BEGIN_DATA_FORMAT`/`END_DATA_FORMAT` and `BEGIN_DATA`/`END_DATA` tags signal
the way the measurements have been formatted.

### Binary Usage

```txt
USAGE:
    cgats [OPTIONS] [FILE]... [SUBCOMMAND]

ARGS:
    <FILE>...    CGATS files

OPTIONS:
    -f, --output-file <output-file>...    Write output to a file
    -h, --help                            Print help information
    -P, --precision <precision>           Number of decimal places to round values to [default: 4]
    -V, --version                         Print version information

SUBCOMMANDS:
    average        Average two or more CGATS files
    colorburst     Convert to and from ColorBurst linearization format
    concatenate    Concatenate rows from multiple CGATS files
    delta          Calculate DeltaE between all values in two compatible CGATS files
    help           Print this message or the help of the given subcommand(s)
    info           Print basic CGATS info
    print          Print the processed CGATS file(s)
    transpose      Transpose chart patches given a layout width
```

Print basic CGATS info to console:

```sh
$ cgats test_files/cgats0.txt
cgats0.txt: Cgats[Cmyk, Lab, Xyz, Spectral(380, 780, 10); 1617]
```

Average values of 3 CGATS files:

```sh
cgats average test_files/cgats{1,2,3}.tsv
```

* Note that these files must have the same `DATA_FORMAT` and sample count.

Concatenate sample sets from multiple CGATS files:

```sh
cgats cat test_files/cgats{1,2,3}.tsv
```

* Note that these files must have the same `DATA_FORMAT`.

Calculate Delta E between each value pair in two CGATS files:

```sh
cgats delta --method 2000 test_files/colorburst{1,2}.lin
```

Calculate Delta E with a statistical analysis report:

```sh
$ cgats delta --report test_files/colorburst{1,2}.lin
Number of Samples: 126
DE Formula: DE2000
E-Factor (95th Percentile): 17.02

OVERALL 100% - (126 colors)
        Average DE: 7.7589
            Max DE: 17.8084
            Min DE: 1.4601
          StDev DE: 4.9772

BEST 95% - (120 colors)
        Average DE: 7.2736
            Max DE: 16.6847
            Min DE: 1.4601
          StDev DE: 4.5895

WORST 5% - (6 colors)
        Average DE: 17.4635
            Max DE: 17.8084
            Min DE: 17.0220
          StDev DE: 0.2825

```

* Note that the two CGATS files must have the same number of samples and must
  each contain `LAB_L`, `LAB_A`, and `LAB_B` fields.

### Binary Installation

First, you'll need to [download and install rust](https://rustup.rs). Then:

```sh
git clone https://gitlab.com/ryanobeirne/cgats
cd cgats
cargo install --example=cgats --path=.
```

### TODO

* Add conversion functions and support for conversion to and from CXF/MXF
* Add support for writing data directly to a CGATS object, as from a spectrophotometer
